//Задание 1
//============================================================
// let  x1 = 2, y1 = 3, x2 = 10, y2 = 5 //площадь равна 16;
// let x1 = 10, y1 = 5, x2 = 2, y2 = 3 //площадь равна 16;
let x1 = -5,
    y1 = 8,
    x2 = 10,
    y2 = 5 //площадь равна 45;
    // let  x1 = 5, y1 = 8, x2 = 5, y2 = 5 //площадь равна 0;
    // let  x1 = 8, y1 = 1, x2 = 5, y2 = 1 //площадь равна 0

let rectangleArea = Math.abs(x1 - x2) * Math.abs(y1 - y2);
console.log(rectangleArea);


//Задание2
//==================================================================


// let a = 13.123456789;
// let b = 2.123;
// let n = 5;


let a = 13.890123;
let b = 2.891564;
let n = 2;

// let a = 13.890123;
// let b = 2.891564; 
// let n = 3;


// целая часть
let aFloor = Math.floor(a);
let bFloor = Math.floor(b);

//дробная часть
let aFract = (a % 1);
let bFract = (b % 1);

let aFractNormalized = Math.round(
    aFract * Math.pow(10, n)
);
let bFractNormalized = Math.round(
    bFract * Math.pow(10, n)
);

console.log('Для a = ', a);
console.log('Для b = ', b);
console.log('Целая часть a: ', aFloor);
console.log('Целая часть b: ', bFloor);
console.log('Дробная часть a: ', aFract);
console.log('Дробная часть b: ', bFract);
console.log('Нормализованная дробная часть a:', aFractNormalized);
console.log('Нормализованная дробная часть b:', bFractNormalized);
//результаты сравнения >, <, ≥, ≤, ===, ≠
console.log('a > b', aFractNormalized > bFractNormalized);
console.log('a < b', aFractNormalized < bFractNormalized);
console.log('a >= b', aFractNormalized >= bFractNormalized);
console.log('a <= b', aFractNormalized <= bFractNormalized);
console.log('a === b', aFractNormalized === bFractNormalized);
console.log('a ≠ b', aFractNormalized !== bFractNormalized);

//Задание 3
//==========================================================================

let n = -1;
let m = 15;

let range = Math.abs(m - n);

let numberInRange = Math.round(Math.random() * range);

let min = Math.min(n, m);

let randomNumber = numberInRange + min;

if ((randomNumber % 2) == 0) {
    randomNumber == min ? ++randomNumber : --randomNumber
}

console.log("Сгенирированное число = " + randomNumber);
